package org.apache.kafka.kafka_stream;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.processor.WordCountProcessor;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;

import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;

public class WordCountStreamP {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        Properties props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "stream-wordcount");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, 0);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());

        // setting offset reset to earliest so that we can re-run the demo code with the same pre-loaded data
        //props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        
        StoreBuilder<KeyValueStore<String, Long>> countStoreSupplier = Stores.keyValueStoreBuilder(
        	//Stores.persistentKeyValueStore("Counts"),
        	Stores.inMemoryKeyValueStore("Counts"),
        	Serdes.String(),
        	Serdes.Long())
        	.withLoggingDisabled(); // disable backing up the store to a changelog topic
        //KeyValueStore<String, Long> countStore = countStoreSupplier.build();
        
        Topology builder = new Topology();
        
		builder.addSource("Source", "stream-in")
		// add the WordCountProcessor node which takes the source processor as its upstream processor
	    .addProcessor("Process", () -> new WordCountProcessor(), "Source")
	    // add the count store associated with the WordCountProcessor processor
	    .addStateStore(countStoreSupplier, "Process")
	    // and the WordCountProcessor node as its upstream processor
	    .addSink("Sink", "stream-out", "Process");

		System.out.println(builder.describe());
		
        KafkaStreams streams = new KafkaStreams(builder, props);

        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-wordcount-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
        streams.cleanUp();
        System.exit(0);

		
	}

}
