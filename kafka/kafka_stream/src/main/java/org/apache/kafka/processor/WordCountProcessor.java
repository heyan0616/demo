package org.apache.kafka.processor;

import java.util.Locale;
import java.util.Optional;
import java.util.stream.Stream;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.state.KeyValueIterator;
import org.apache.kafka.streams.state.KeyValueStore;

public class WordCountProcessor implements Processor<String, String> {

	  private ProcessorContext context;
	  
	  private KeyValueStore<String, Long> kvStore;

	  @Override
	  @SuppressWarnings("unchecked")
	  public void init(ProcessorContext context) {
	      // keep the processor context locally because we need it in punctuate() and commit()
	      this.context = context;

	      // retrieve the key-value store named "Counts"
	      this.kvStore = (KeyValueStore<String, Long>) context.getStateStore("Counts");

	      // schedule a punctuate() method every 1000 milliseconds based on stream-time
	      this.context.schedule(1000, PunctuationType.STREAM_TIME, (timestamp) -> {
	          KeyValueIterator<String, Long> iter = this.kvStore.all();
	          while (iter.hasNext()) {
	              KeyValue<String, Long> entry = iter.next();
	              
	              System.out.println("KEY: "  + entry.key + " VALUE: " + entry.value);
	              
	              context.forward(entry.key, entry.value.toString());
	          }
	          iter.close();

			  // commit the current processing progress
			  context.commit();
	      });

	  }

	  public void punctuate(long timestamp) {
	      // this method is deprecated and should not be used anymore
	  }

	  @Override
	  public void close() {
	      // close any resources managed by this processor
	      // Note: Do not close any StateStores as these are managed by the library
		  this.kvStore.close();
	  }

	  @Override
	  public void process(String key, String value) {
		  // TODO Auto-generated method stub

          String[] words = value.split(";");

          for (String word : words) {
              Long oldValue = this.kvStore.get(word);
              
              System.out.println("word = "+word+" ; oldvalue = "+oldValue); 

              if (oldValue == null) {
                  this.kvStore.put(word, (long) 1);
                  
              } else {
                  this.kvStore.put(word, oldValue + 1);
                  
              }
              
          }
          context.commit();	
	  }

}
