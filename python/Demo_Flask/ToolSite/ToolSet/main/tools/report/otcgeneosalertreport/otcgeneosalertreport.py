import subprocess
import os

def runOtcGeneosAlertReport(app_name,start_date,end_date,username,password,rootpath):
    print(app_name,start_date,end_date,username,'********')
    try:
        ret = subprocess.Popen(['java', '-jar', rootpath+r'\Toolset\main\tools\report\otcgeneosalertreport\scripts\GeneosReport.jar', username, password, app_name, start_date, end_date]
                                ,stdout=subprocess.PIPE
                                ,stderr=subprocess.PIPE)
        output, error = ret.communicate()
        # if output:
        #     print ("ret> ", test.returncode)
        #     print ("OK> output ", output)
        if error:
            error_message = str(error.strip())
            return error_message
        else:
            return 'external_call_success'
    except subprocess.CalledProcessError as e:
        return e.output