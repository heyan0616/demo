import subprocess
import os

def runCognosMothlyReport(factdate,username,password,rootpath):
    print(factdate, username, '********')

    try:
        ret = subprocess.Popen(['java', '-jar', rootpath+r'\Toolset\main\tools\auto\cognos\scripts\cognos.jar', factdate, username, password])
        output, error = ret.communicate()
        # if output:
        #     print ("ret> ", test.returncode)
        #     print ("OK> output ", output)
        if error:
            error_message = str(error.strip())
            return error_message
        else:
            return 'external_call_success'
    except subprocess.CalledProcessError as e:
        return e.output

    # subprocess.call(['java', '-jar', 'Blender.jar'])
    # to include arguments when executing a.jar file with subprocess.call(), simply append them to the end of the method separated by commas example: subprocess.call(
    #     ['java', '-jar', 'Blender.jar', 'arg1', 'arg2', 'however_many_args_you_need'])