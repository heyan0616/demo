from flask import Flask,request, redirect,url_for, render_template
from flask import send_from_directory
from .tools.auto.cognos.cognosmonthlyreport import runCognosMothlyReport
from .tools.auto.test.test import runTest
from .tools.report.otcgeneosalertreport.otcgeneosalertreport import runOtcGeneosAlertReport

from config import config
from . import main

rootpath = config['rootpath']

# main site
@main.route('/', methods=['GET', 'POST'])
@main.route('/index', methods=['GET', 'POST'])
@main.route('/index.html', methods=['GET', 'POST'])
def show_index():

    # if there is POST data
    if request.method == 'POST':
        if ('report_geneos_alert_for_app' in request.form):
            app_name = request.form['app_name']
            start_date = request.form['start_date']
            end_date = request.form['end_date']
            username = request.form['username']
            password = request.form['password']
            try:
                # call the function
                result = runOtcGeneosAlertReport(app_name,start_date,end_date,username,password,rootpath)
                # url redirect
                if result == 'external_call_success':
                    return redirect(url_for('main.download_file_report_otcGA', filename='OTCGeneosAlertReport.xls'))
                else:
                    return render_template('error.html', error='Error:' + result)
            except Exception as e:
                print('Error:', e)
                return render_template('error.html', error='Error:' + e)

        elif ('auto_cognos_monthly_report' in request.form):
            fact_date = request.form['fact_date']
            username = request.form['username']
            password = request.form['password']
            try:
                # call the function
                result = runCognosMothlyReport(fact_date,username,password,rootpath)
                # url redirect
                if result == 'external_call_success':
                    # redirect to download file
                    return redirect(url_for('main.download_file_auto_cognosM', filename='mth_ddaint.xls'))
                else:
                    return render_template('error.html', error='Error:' + result)

            except Exception as e:
                print('Error:', e)

        elif ('auto_test' in request.form):
            filename = request.form['filename']
            try:
                # call the function
                result = runTest(filename,rootpath)

                if result == 'external_call_success':
                    # redirect to download file
                    return redirect(url_for('main.download_file_auto_test', filename = filename+'.xls'))
                else:
                    return render_template('error.html', error='Error:' + result)

            except Exception as e:
                print('Error:', e)
                return render_template('error.html', error='Error:' + e)
            finally:
                pass

        else:
            return render_template('index.html')
    else:
        return render_template('index.html')


# download file for auto test module
@main.route("/auto/test/download/<filename>", methods=['GET'])
def download_file_auto_test(filename): # 2 parameters, first is the local path, second is the file name(with .xxx) directory = os.getcwd() # if it is not in local the same path
    path = rootpath+'\\Toolset\\main\\tools\\auto\\test\\scripts\\'
    return send_from_directory(path, filename, as_attachment=True) # as_attachment：True

# download file for auto cognos monthly report module
@main.route("/auto/cognos/download/<filename>", methods=['GET'])
def download_file_auto_cognosM(filename): # 2 parameters, first is the local path, second is the file name(with .xxx) directory = os.getcwd() # if it is not in local the same path
    path = rootpath+'\\Toolset\\main\\tools\\auto\\cognos\\scripts\\'
    return send_from_directory(path, filename, as_attachment=True) # as_attachment：True

# download file for auto cognos monthly report module
@main.route("/report/otcgeneosalertreport/download/<filename>", methods=['GET'])
def download_file_report_otcGA(filename): # 2 parameters, first is the local path, second is the file name(with .xxx) directory = os.getcwd() # if it is not in local the same path
    path = rootpath+'\\Toolset\\main\\tools\\report\\otcgeneosalertreport\\scripts\\'
    return send_from_directory(path, filename, as_attachment=True) # as_attachment：True

# about site
@main.route('/about.html')
@main.route('/about')
def show_about():
    return render_template('about.html')

# contact site
@main.route('/contact.html', methods=['GET', 'POST'])
@main.route('/contact', methods=['GET', 'POST'])
def show_contact():
    return render_template('contact.html')

# blog site
@main.route('/blog.html')
@main.route('/blog')
def show_blog():
    return render_template('blog.html')

# details site
@main.route('/details.html')
@main.route('/details')
def show_details():
    return render_template('details.html')

# error site
@main.route('/error.html', methods=['GET'])
@main.route('/error', methods=['GET'])
def show_error():
    return render_template('error.html')

# applications site
@main.route('/applications.html')
@main.route('/applications')
def show_portfolio():
    return render_template('applications.html')
