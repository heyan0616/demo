import requests
from bs4 import BeautifulSoup
import re
import os

header = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q=0.8',
        'Connection': 'keep-alive',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 '
                      '(KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
}



# get the course list for each subject
def get_course_list(url):
    response = requests.get(url, header)
    response.encoding = 'utf-8'

    soup = BeautifulSoup(response.text, 'lxml')
    course_list = soup.find_all(class_='smallCourseCatalog')[0].find_all('li')
    course_list_format = []

    for course in course_list:
        course_dic = {}
        course_dic['course_url'] = 'http://m.maiziedu.com' + course.find_all(name='a')[0]['href']
        course_dic['course_duration'] = course.find_all(name='span')[0].string.strip()
        a, b = course.stripped_strings
        course_dic['course_name'] = b

        # get the video url
        response = requests.get(course_dic['course_url'], header)
        response.encoding = 'utf-8'
        soup = BeautifulSoup(response.text, 'lxml')
        course_dic['course_videourl'] = soup.find_all(class_='course-banner')[0].find_all('video')[0]['src']

        course_list_format.append(course_dic)
    return course_list_format





# main ()
urls = ['http://m.maiziedu.com/course/ml-all/0-1/',
       'http://m.maiziedu.com/course/ml-all/0-2/',
       'http://m.maiziedu.com/course/ml-all/0-3/']

last_list = []

for url in urls:
    response = requests.get(url, header)
    response.encoding = 'utf-8'

    all_list_format = []

    soup = BeautifulSoup(response.text, 'lxml')
    all_list = soup.find_all(name='article', class_='course_list')[0].find_all(name='li')

    for list in all_list:
        main_dic = {}
        course_lists = {}
        main_dic['main_url'] = 'http://m.maiziedu.com' + list.find_all(name='a')[0]['href']
        main_dic['main_name'] = list.find_all(name='strong')[0].string.strip()
        #main_dic['main_desc'] = list.find_all(name='p')[0].string.strip()

        #get the course lists
        course_lists = get_course_list(main_dic['main_url'])
        main_dic['course_list'] = course_lists

        all_list_format.append(main_dic)


    main_path = r'C:\Users\admin\Desktop\kecheng\machinelearning'
    for list_format in all_list_format:
        folder = main_path + '\\' + list_format['main_name']
        if not os.path.exists(folder):
            os.makedirs(folder)

        for course in list_format['course_list']:

            last_dic = {}
            last_dic['url'] = list_format['main_url']
            last_dic['subject'] = list_format['main_name']
            last_dic['course_name'] = course['course_name']
            last_dic['video_url'] = course['course_videourl']
            last_list.append(last_dic)

            video_path = folder + '\\' + course['course_name'] + '.mp4'
            r = requests.get(course['course_videourl'], stream=True)
            f = open(video_path, "wb")
            for chunk in r.iter_content(chunk_size=512):
                if chunk:
                    f.write(chunk)

            print (last_dic)

#----------------------------------requirments----------------------------------
# 介绍： 这是用来批量下载麦子学院的视频 （m.maizi.xxx 这个网站可下载付费视频）
# 主要是 requests + beautifulsoup的使用； 同时下载文件到本地文件夹
#--------------------------------------------------------------------------------


# -------------------------------------------上面是循环下载整个主题下的视频，下面的function是下载单个课程的代码
def down_single():

	header = {
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
			'Accept-Encoding': 'gzip, deflate, br',
			'Accept-Language': 'zh-CN,zh;q=0.8',
			'Connection': 'keep-alive',
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 '
						  '(KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
	}

	url = 'http://m.maiziedu.com/course/313/'


	response = requests.get(url, header)
	response.encoding = 'utf-8'

	soup = BeautifulSoup(response.text, 'lxml')
	course_list = soup.find_all(class_='smallCourseCatalog')[0].find_all('li')
	course_list_format = []

	for course in course_list:
		course_dic = {}
		course_dic['course_url'] = 'http://m.maiziedu.com' + course.find_all(name='a')[0]['href']
		course_dic['course_duration'] = course.find_all(name='span')[0].string.strip()
		a, b = course.stripped_strings
		course_dic['course_name'] = b

		# get the video url
		response = requests.get(course_dic['course_url'], header)
		response.encoding = 'utf-8'
		soup = BeautifulSoup(response.text, 'lxml')
		course_dic['course_videourl'] =  soup.find_all(class_='course-banner')[0].find_all('video')[0]['src']

		course_list_format.append(course_dic)

	main_path = r'D:\Videos\flask入门'
	if not os.path.exists(main_path):
		os.makedirs(main_path)

	for course in course_list_format:

		video_path = main_path + '\\' + course['course_name'] + '.mp4'
		r = requests.get(course['course_videourl'], stream=True)
		f = open(video_path, "wb")
		for chunk in r.iter_content(chunk_size=512):
			if chunk: 
				f.write(chunk)

		print (course)




