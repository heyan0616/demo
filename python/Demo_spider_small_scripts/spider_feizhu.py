
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import pandas as pd
from datetime import datetime


def datelist(beginDate, endDate):
    # beginDate, endDate是形如‘20160601’的字符串或datetime格式
    date=[datetime.strftime(x,'%Y-%m-%d') for x in list(pd.date_range(start=beginDate, end=endDate))]
    return date


def spider_flight(depCity,arrCity,date):
    # -------------------------------------notes--------------------------------------
    # 介绍： Selenium+chromedriver 抓取JS动态网页
    # 提示： chromedriver.exe 放在项目目录下； 最新版本的Selenium已不再支持PhantomJS； 设置成headless模式，防止chrome弹出来
    # 系统需要 chrome >= 59
    # Windows 系统需要 chrome >= 60
    # Python3.6
    # Selenium==3.4.*
    # ChromeDriver==2.31
    # --------------------------------------------------------------------------------

    header = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.8',
            'Connection': 'keep-alive',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 '
                          '(KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
    }

    flightlist_one = []

    # url = 'https://sjipiao.fliggy.com/flight_search_result.htm?tripType=0&depCity=HGH&arrCity=SIA&depDate=2018-05-24&searchBy=1280'
    url = 'https://sjipiao.fliggy.com/flight_search_result.htm?tripType=0&depCity='+depCity+'&arrCity='+arrCity+'&depDate='+date+'&searchBy=1280'

    # chrome headless settings - avoid chrome pop up
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    chrome_options.binary_location = r'C:\Program Files (x86)\Google\Chrome\Application\chrome.exe'

    driver=webdriver.Chrome(chrome_options=chrome_options)

    driver.get(url)
    data = driver.page_source.encode('utf-8')

    driver.close()

    # parser the page source and return them
    soup = BeautifulSoup(data, 'lxml')

    # flight list
    flit_items = soup.find_all('div', class_='flight-list-item clearfix J_FlightItem')
    for flight in flit_items:
        flightdic = {}
        flightdic['日期'] = date
        flightdic['航空公司'] = flight.find_all('span' , class_='J_line J_TestFlight')[0].string
        flightdic['起飞时间'] = date + ' ' + flight.find_all('p', class_='flight-time-deptime')[0].string
        flightdic['到达时间'] = date + ' ' + flight.find_all('span', class_='s-time')[0].string
        flightdic['价格'] = flight.find_all('span', class_='J_FlightListPrice')[0].string
        flightdic['起飞'] = flight.find_all('p', class_='port-dep')[0].string
        flightdic['到达'] = flight.find_all('p', class_='port-arr')[0].string

        if len(flight.find_all('a', class_='link-dot J_Stop')) == 0:
            flightdic['经停'] = 'N'
        else:
            flightdic['经停'] = 'Y'

        flightlist_one.append(flightdic)

    # for list_one in flightlist_one:
    #     print ('航空公司:' + list_one['航空公司'],
    #            '起飞时间:' + list_one['起飞时间']
    #        )

    # return one day's flights
    return flightlist_one


def main():

    flights_list_all = []   # finally all flights will be installed into one list

    start_date = '2018-05-05'   # start search date
    end_date = '2018-05-06'     # end search date
    from_place_code = 'HGH'     # from place
    to_place_code = 'SIA'       # end place

    for current_date in datelist(start_date,end_date):
        flights_list_one = spider_flight(from_place_code,to_place_code,current_date)
        flights_list_all.append(flights_list_one)

    for list_one_day in flights_list_all:
        for dic_one in list_one_day:
            print (
                '日期：' + dic_one['日期'] + '      ',
                '航空公司:' + dic_one['航空公司'] + '      ',
                '起飞时间:' + dic_one['起飞时间'] + '      ',
                '到达时间:' + dic_one['到达时间'] + '      ',
                '价格:' + dic_one['价格'] + '      ',
                '起飞:' + dic_one['起飞'] + '      ',
                '到达:' + dic_one['到达'] + '      ',
                '经停:' + dic_one['经停'] + '      '
               )


main ()