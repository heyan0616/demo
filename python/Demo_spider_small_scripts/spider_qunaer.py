import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait


header = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'zh-CN,zh;q=0.8',
        'Connection': 'keep-alive',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 '
                      '(KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36'
}


url = 'https://flight.qunar.com/site/oneway_list.htm?searchDepartureAirport=%E6%9D%AD%E5%B7%9E&searchArrivalAirport=%E8%A5%BF%E5%AE%89&searchDepartureTime=2018-05-10&searchArrivalTime=2018-05-05&nextNDays=0&startSearch=true&fromCode=HGH&toCode=SIA&from=qunarindex&lowestPrice=null'
#url = 'http://www.baidu.com'

browser = None


try:

    # set proxy
    proxy = "proxy.xxxx.com"
    port = 80
    profile = webdriver.FirefoxProfile()
    profile.set_preference('network.proxy.ssl_port', int(port))
    profile.set_preference('network.proxy.ssl', proxy)
    profile.set_preference('network.proxy.http_port', int(port))
    profile.set_preference('network.proxy.http', proxy)
    profile.set_preference('network.proxy.type', 1)

    # set options
    firefox_options = Options()
    firefox_options.add_argument('--headless')
    firefox_options.add_argument('--disable-gpu')

    # set the webdriver value
    browser = webdriver.Firefox(firefox_options=firefox_options, firefox_profile=profile)

    browser.get(url)
    WebDriverWait(browser, 15) # 这里如果不设置等待，只能取出一条数据。。 原因有待查看
    data = browser.page_source
    print (data)

except TimeoutException as te:
    print ('timeout')
except Exception as ex:
    print (ex)
finally:
    browser.quit()



# soup = BeautifulSoup(data, 'lxml')
#
# flit_items = soup.find_all('div', class_='flight-list-item clearfix J_FlightItem')
# for item in flit_items:
#     print (item)


#----------------------------------requirments----------------------------------
# 介绍： Selenium+firefox 抓取JS动态网页
# 提示： geckodriver.exe (firefox的deriver) 放在项目目录下； 最新版本的Selenium已不再支持PhantomJS； 设置成headless模式，防止firefox弹出来
#--------------------------------------------------------------------------------
