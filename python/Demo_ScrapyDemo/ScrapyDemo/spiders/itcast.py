# -*- coding: utf-8 -*-
import scrapy

from ScrapyDemo.items import ScrapydemoItem


class ItcastSpider(scrapy.Spider):
    name = 'itcast'
    allowed_domains = ['tencent.cn']
    baseURL = 'http://hr.tencent.com/position.php?&start='
    offset = 0
    start_urls = [baseURL + str(offset)]

    def parse(self, response):

        # 提取response数据
        node_list = response.xpath("//tr[@class='even'] | //tr[@class='odd'] ")

        for node in node_list:

            # 构建item对象，用来保存数据
            item = ScrapydemoItem()

            # 提取数据，并将提取的unicode字符串编码为utf-8
            item['positionName'] = node.xpath("./td[1]/a/text()").extract()[0].encode("utf-8")

            item['positionLink'] = node.xpath("./td[1]/a/@href").extract()[0].encode("utf-8")

            if len(node.xpath("./td[2]/text()")) == 0:
                item['positionType'] = node.xpath("./td[2]/text()").extract()[0].encode("utf-8")
            else:
                item['positionType'] = ""

            item['peopleNumber'] = node.xpath("./td[3]/text()").extract()[0].encode("utf-8")

            item['workLocation'] = node.xpath("./td[4]/text()").extract()[0].encode("utf-8")

            item['publishTime'] = node.xpath("./td[5]/text()").extract()[0].encode("utf-8")

            # yield - 返回数据后还能回来继续执行代码
            yield item

        # 第一种写法，拼接URL，适用场景： 页面没有可以点击的请求链接，必须通过拼接url才能获取响应
        # if self.offset < 100:
        #     self.offset += 10
        #     url = self.baseURL + str(self.offset)
        #     yield scrapy.Request(url, callback=self.parse)

        # 第二种写法，直接从response获取需要爬取的链接，并发送请求处理，直到链接全部提取完
        if len(response.xpath("//a[@class='noactive' and @class='next']")) == 0:

            url = response.xpath("//a[@id='next']/@href").extract()[0]

            yield scrapy.Request("http://hr.tencent.com/" + url, callback=self.parse)

